import './js/some';
import './scss/main.scss';


function requireAll(r) {
  r.keys().forEach(r);
}

requireAll(require.context('./img/svg/', true, /\.svg$/));