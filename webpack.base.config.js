const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

const PATHS = {
  src: path.join(__dirname, './src'),
  dist: path.join(__dirname, './dist'),
  assets: 'assets/'
}

module.exports = {

  externals: {
    paths: PATHS
  },

  // entry point
  entry: {
    main: `${PATHS.src}/index.js` // файл, в котором подключаются все модули
  },

  // output point
  output: {
    filename: `${PATHS.assets}js/[name].js`, // файл, куда будут собиратся все модули
    path: PATHS.dist, // папка, куда Webpack будет размещать готовый файл
    publicPath: ""
  },

  module: {
    rules: [
      // JS
      {
        test: /\.js$/, // // проверка, является ли файл js-файлом (с раширением js)
        loader: 'babel-loader', // обработчик, который будет обрабатывать js файлы
        exclude: '/node_modules/' // исключить файлы из обработки
      },

      // SCSS, SASS, CSS
      {
        test: /\.(sass|scss|css)$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: `${PATHS.src}/js/postcss.config.js`
              }
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },

      // IMAGES
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        }
      },

      // FONTS
      {
        test: /\.(woff|woff2|ttf|otf)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        }
      },

      {
        test: /\.svg$/,
        loader: 'svg-sprite-loader',
        options: {
          extract: true,
          spriteFilename: `${PATHS.assets}img/icons.svg`, // this is the destination of your sprite sheet
        }
      },
    ]
  },

  plugins: [
    new CleanWebpackPlugin(['./dist']),
    new HtmlWebpackPlugin({
      template: `${PATHS.src}/index.html`, // путь к шаблону
      filename: 'index.html', // файл для записи (можно указать подкаталог (например, assets/admin.html)
      inject: true, // true или 'body' все ресурсы js будут размещены внизу элемента body. 'head' поместит скрипты в элемент head      
    }),
    new MiniCssExtractPlugin({
      filename: `${PATHS.assets}css/[name].css`, // файл в папке dist, куда будут собираться все стили       
    }),    
    new CopyWebpackPlugin([{
        from: `${PATHS.src}/img`,
        to: `${PATHS.assets}img`,
      },
      {
        from: `${PATHS.src}/fonts`,
        to: `${PATHS.assets}fonts`
      }
      // { from: PATHS.src + '/static' },
    ]),
    new SpriteLoaderPlugin({
      plainSprite: true
    }),    
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ]

}