const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.config');
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');

const buildWebpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  plugins: [
    new ImageminPlugin({ 
      plugins: [
        imageminMozjpeg({
          quality: 70,
          progressive: true
        }),
        imageminPngquant({
          speed: 3,
          quality: [0.6, 0.8]
        })      
      ]
    }),  
  ]
})

module.exports = new Promise((resolve, reject) => {
  resolve(buildWebpackConfig)
})